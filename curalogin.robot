*** Settings ***
Documentation    Test d'accès à la page de login
Library    SeleniumLibrary

*** Variables ***
${URL}    https://katalon-demo-cura.herokuapp.com
${Browser}    firefox
${login}    John Doe
${password}    ThisIsNotAPassword

*** Test Cases ***
Login page test case

    # Open firefox browser and Go to Website #
    Open Browser   ${URL}    ${Browser}

    # Maximize browser window #
    Maximize Browser Window

    # Click on Make appointment #
    Click Element    id:btn-make-appointment

    # Fill in Username #
    Input Text    id:txt-username    ${login}

    # Fill in Password #
    Input Password    id:txt-password    ${password}

    # Clik on Login #
    Click Button    id:btn-login

    # Assert page #
    Title Should Be    CURA Healthcare Service

    # Book appointement #
    Click Element    xpath=//*[@id="combo_facility"]/option[2]
    Element Should Be Visible    xpath=//*[@id="combo_facility"]/option[2]
    Select Checkbox    id:chk_hospotal_readmission
    Checkbox Should Be Selected    id:chk_hospotal_readmission
    Click Element    id:radio_program_medicaid
    Radio Button Should Be Set To    programs    Medicaid
    Input Text    id:txt_visit_date    02/07/2022
    Textfield Should Contain    id:txt_visit_date    02/07/2022
    Input Text    id:txt_comment    Ceci est un commentaire
    Textarea Should Contain    id:txt_comment    Ceci est un commentaire

    # Valid appointement #
    Click Button    id:btn-book-appointment

    # Wait page #
    Wait Until Page Contains Element    xpath=//*[@id="summary"]/div/div/div[1]/h2

    # Check appointement infos #
    #Element Should Be Visible    xpath=//*[@id="summary"]/div/div/div[1]/h2
    Element Should Be Visible    tag:h2
    Element Should Be Visible    xpath=//*[@id="summary"]/div/div/div[1]/p
    Element Text Should Be    id:facility    Hongkong CURA Healthcare Center
    Element Text Should Be    id:hospital_readmission    Yes
    Element Text Should Be    id:program    Medicaid
    Element Text Should Be    id:visit_date    02/07/2022
    Element Text Should Be    id:comment    Ceci est un commentaire

    # Back to Homepage #
    Click Element    xpath=//*[@id="summary"]/div/div/div[7]/p/a

    # Wait page #
    Wait Until Page Contains Element    id:btn-make-appointment

    # Check Homepage #
    Element Should Be Visible    xpath=//*[@id="top"]/div/h1

    # Wait #
    Sleep    2

    # Close Browser #
    Close Browser

Connection with password empty

    # Open firefox browser and Go to Website #
    Open Browser   ${URL}    ${Browser}

    # Maximize browser window #
    Maximize Browser Window

    # Click on Make appointment #
    Click Element    id:btn-make-appointment

    # Fill in Username #
    Input Text    id:txt-username     ${login}

    # Clik on Login #
    Click Button    id:btn-login

    # Assert message #
    Element Should Be Visible    xpath=//p[contains(text(),'Login failed! Please ensure the username and password are valid.')]

    # Wait #
    Sleep    2

    # Close Browser #
    Close Browser

Connection with false credentials 

    # Open firefox browser and Go to Website #
    Open Browser   ${URL}    ${Browser}

    # Maximize browser window #
    Maximize Browser Window

    # Click on Make appointment #
    Click Element    id:btn-make-appointment

    # Fill in Username #
    Input Text    id:txt-username     ${login}

    # Fill in Password #
    Input Password    id:txt-password    ${password}

    # Clik on Login #
    Click Button    id:btn-login

    # Assert message #
    Element Should Be Visible    xpath=//p[contains(text(),'Login failed! Please ensure the username and password are valid.')]

    # Wait #
    Sleep    2

    # Close Browser #
    Close Browser